# Mitt program

Se [oppgaveteksten](./OPPGAVETEKST.md) til semesteroppgave 2. Denne README -filen kan du endre som en del av dokumentasjonen til programmet, hvor du beskriver for en bruker hvordan programmet skal benyttes.

Dette er et mamory-spill. Memory er et spill der man skla finne to og to like figurer blant et gitt antall kort. 
Kortene begynner med å ligge med baksiden opp, ettersom man trykker på kortene vil de snus to av gangen. 
Dersom kortene er like vil de bli stående. Når man har fått alle kortene til å snu seg vil man ha vunnet spillet. 

Man setter i gang programmet ved å trykke på trekanten øverst til høyre i VSCode. Da vil et vindu poppe opp på
hjemskjermen. Da er det bare å trykke på skjermen for å starte spillet. I det man trykker begynner klokken å gå.
Når man har trykket fram alle kortene vil tiden stoppes og man får vite hvor lang tid man har brukt. 