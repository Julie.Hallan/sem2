package no.uib.inf101.sem2.ModelTests;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.Model.MemoryBoard;
import no.uib.inf101.sem2.grid.CellPosition;

import static org.junit.jupiter.api.Assertions.*;

public class MemoryBoardTest {
    
    @Test
    public void testConstructor() {
        // Arrange
        int rows = 2;
        int cols = 3;

        // Act
        MemoryBoard board = new MemoryBoard(rows, cols);

        // Assert
        assertEquals(rows, board.rows());
        assertEquals(cols, board.cols());
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                CellPosition cellPosition = new CellPosition(i, j);
                assertEquals('0', board.get(cellPosition));
            }
        }
    }
}
