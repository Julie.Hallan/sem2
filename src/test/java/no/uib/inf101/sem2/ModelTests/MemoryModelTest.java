package no.uib.inf101.sem2.ModelTests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.Model.GameState;
import no.uib.inf101.sem2.Model.MemoryBoard;
import no.uib.inf101.sem2.Model.MemoryModel;
import no.uib.inf101.sem2.grid.CellPosition;

public class MemoryModelTest {

    private MemoryModel memoryModel;
    private MemoryBoard memoryBoard;
    private ArrayList<CellPosition> cellPositions;

    @Test
    public void testOpenCard() {
        memoryBoard = new MemoryBoard(4, 4);
        memoryModel = new MemoryModel(memoryBoard);
        cellPositions = new ArrayList<>();
        cellPositions.add(new CellPosition(0, 0));
        cellPositions.add(new CellPosition(0, 1));
    
        memoryModel.openCard(cellPositions.get(0));
        assertEquals(memoryModel.cardValue(cellPositions.get(0)), memoryBoard.get(cellPositions.get(0)));
    }

    @Test
    public void testCloseCard() {
        memoryBoard = new MemoryBoard(4, 4);
        memoryModel = new MemoryModel(memoryBoard);
        cellPositions = new ArrayList<>();
        cellPositions.add(new CellPosition(0, 0));
        cellPositions.add(new CellPosition(0, 1));
        memoryModel.closeCard(cellPositions.get(0));
        assertEquals('0', memoryBoard.get(cellPositions.get(0)));
    }

    @Test
    public void testCompareAndAddValues() {
        memoryBoard = new MemoryBoard(4, 4);
        memoryModel = new MemoryModel(memoryBoard);
        cellPositions = new ArrayList<>();
        cellPositions.add(new CellPosition(0, 0));
        cellPositions.add(new CellPosition(0, 1));

        memoryModel.openCard(cellPositions.get(0));
        memoryModel.openCard(cellPositions.get(1));
        memoryModel.compareAndAddValues(cellPositions.get(0), cellPositions.get(1));
        assertEquals(1, memoryModel.addScore());
    }

    @Test
    public void testGameStateCheckerGameStart() {
        memoryModel = new MemoryModel(memoryBoard);
        assertEquals(GameState.GAME_START, memoryModel.getGameState());
    }

    //Kan jeg gjøre noe med denne når jeg ikke har instansvariabler?
    @Test
    public void testGameStateCheckerGameOver() {
        int score = 0;
        memoryModel = new MemoryModel(memoryBoard);
        for (int i = 0; i < 8; i++) {
            memoryModel.addScore();
        }
        memoryModel.gameStateChecker();
        assertEquals(GameState.GAME_OVER, memoryModel.getGameState());
    }

    @Test
    public void testIncreaseTimeUsed() {
        memoryModel = new MemoryModel(memoryBoard);
        memoryModel.increaseTimeUsed();
        assertEquals(1, memoryModel.getTimeUsed());
    }

    @Test
    public void testMillisecsInClock() {
        memoryModel = new MemoryModel(memoryBoard);
        assertEquals(1000, memoryModel.millisecsInClock());
    }
}