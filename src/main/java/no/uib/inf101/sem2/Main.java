package no.uib.inf101.sem2;

import no.uib.inf101.sem2.Controller.MemoryController;
import no.uib.inf101.sem2.Model.MemoryBoard;
import no.uib.inf101.sem2.Model.MemoryModel;
import no.uib.inf101.sem2.view.MemoryView;
import javax.swing.JFrame;


public class Main {
  public static void main(String[] args) {

    MemoryBoard board = new MemoryBoard(4, 4);
    MemoryModel model = new MemoryModel(board);
    MemoryView view = new MemoryView(model);

    new MemoryController(model, view);

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("INF101");
    frame.setContentPane(view);
    frame.pack();
    frame.setVisible(true);
  }
}
