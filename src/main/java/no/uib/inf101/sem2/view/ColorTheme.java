package no.uib.inf101.sem2.view;

import java.awt.Color;

public interface ColorTheme {

    /**
     * Returns the color corresponding to the character specified by the parameter c.
     * @param c specified which color an object should have
     * @return the color corresponding to c
     */
    public Color getCellColor();
    
    /**
     * Returns an invisible frame color.
     * @return frame color
     */
    public Color getFrameColor();

    /**
     * Returns an invisible background color
     * @return background color
     */
    public Color getBackgroundColor();

    /**
     * Returns a light pink overlay for the game over view at the end. 
     * @return overlay color
     */
    public Color getGameOverPageColor();

    /**
     * Returns the color white for the game over sign at the end of the game.
     * @return text color
     */
    public Color getGameOverTextColor();

     /**
     * Returns the color white for the game over sign at the end of the game.
     * @return text color
     */
    public Color getGameOverTextColor2();
    
}
