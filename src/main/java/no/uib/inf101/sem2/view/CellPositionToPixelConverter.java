package no.uib.inf101.sem2.view;
    
import java.awt.geom.Rectangle2D;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridDimension;

public class CellPositionToPixelConverter {

  Rectangle2D box;
  GridDimension gd;
  Double margin;

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box; 
    this.gd = gd;
    this.margin = margin;
  
  }
  /**
   * Calculates the size of the cells to fit the board, given a specified number of rows and columns. 
   * @param CellPosition the position of the cell to be calculated
   * @return Rectangle2D object that represents the boundries of the specified CellPosition
   */
  public Rectangle2D getBoundsForCell(CellPosition CellPosition) {
    double width = box.getWidth();
    double height = box.getHeight();
    double rows = gd.rows();
    double cols = gd.cols();

    double cellWidth = (width - this.margin * cols - this.margin) / cols;
    //(width - ((this.margin * (cols + 1))) / cols); // finner vellevidden fra bredden til bakgrunden
    double cellHeight = (height - this.margin * rows - this.margin) / rows ;
    //(height - ((this.margin * (rows + 1))) / rows);

    double cellX = box.getX() + this.margin + (cellWidth + this.margin) * CellPosition.col();
    //box.getX() + (this.margin * (CellPosition.col() + 1)) + (cellWidth * CellPosition.col()); // finner x og y verdi
    double cellY = box.getY() + this.margin + (cellHeight + this.margin) * CellPosition.row();
    //box.getY() + (this.margin * (CellPosition.row() + 1)) + (cellHeight * CellPosition.row());

    Rectangle2D rectangle = new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight); 
    return rectangle;
  }
}