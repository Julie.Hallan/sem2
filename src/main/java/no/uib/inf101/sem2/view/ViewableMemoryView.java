package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.Model.GameState;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;

public interface ViewableMemoryView {

    /**
     * returns the dimensions of the game board as a MemoryBoard object.
     * @return the dimensions of the game board.
     */
    GridDimension getDimension();

    /**
     * Returns an Iterable of GridCell objects that represent the pictures
     * on the game board.
     *
     * @return an iterable of grid cells representing the pictures on the game board
     */
    Iterable<GridCell<Character>> getPictures();

    /**
     * Returns the current game state.
     * @return object of type GameState.
     */
    GameState getGameState();

    /**
     * Updates the gameState if the player has matched all pictures, 
     * sets the game state to GAME_OVER.
     */
    void gameStateChecker();
 
    /**
     * Increases the time used for the current game by one.
     * This method is called once per second to update the game timer.
     */
    void increaseTimeUsed();

    /**
     * Returns the time that has passed when the function is called.
     * @return passed time.
     */
    int getTimeUsed();
}
