package no.uib.inf101.sem2.view;

import java.awt.Color;


public class DefaultColorTheme implements ColorTheme {

    public Color getCellColor() {
        return Color.decode("#ff687e");
        //return Inf101Graphics.loadImageFromResources("/inf101_duck.png")
           // en farge
    }

    public Color getFrameColor(){
        return (new Color(0, 0, 0, 0));
    }

    public Color getBackgroundColor(){
        return null;
    }

    public Color getGameOverPageColor(){
        return (new Color(255, 182, 193, 128));
    }

    @Override
    public Color getGameOverTextColor() {
        return Color.WHITE;
    }

    @Override
    public Color getGameOverTextColor2() {
        return Color.PINK;
    }


}