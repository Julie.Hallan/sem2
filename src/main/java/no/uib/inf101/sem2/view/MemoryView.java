package no.uib.inf101.sem2.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JPanel;

import no.uib.inf101.sem2.Controller.MemoryController;
import no.uib.inf101.sem2.Model.GameState;
import no.uib.inf101.sem2.Model.MemoryModel;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;

public class MemoryView extends JPanel{
    ViewableMemoryView view;
    int numberOfCards = 16;
    private ColorTheme colorTheme;
    private static int width = 1500;
    private static int height = 1000;
    public MemoryModel model;
    public MemoryController controller;
    private ArrayList<BufferedImage> pictureList;
    
    public MemoryView(ViewableMemoryView view) {
        this.view = view;
        this.setPreferredSize(new Dimension(width, height));
        this.colorTheme = new DefaultColorTheme();
        this.pictureList = new ArrayList<BufferedImage>();
        for (int i = 1; i < 9; i++){
            BufferedImage picture = Inf101Graphics.loadImageFromResources("/picture"+i+".jpg");
            pictureList.add(picture);
        }
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        drawGame(g2);
    }

    private void drawGame(Graphics2D g2){
            Color pageColor = colorTheme.getGameOverPageColor();
            Color textColor = colorTheme.getGameOverTextColor();
            Color textColor2 = colorTheme.getGameOverTextColor2();
        if (view.getGameState() == GameState.GAME_START){
            g2.setColor(pageColor);
            g2.fillRect(0, 0, this.getWidth(), this.getHeight());
            g2.setColor(textColor);
            g2.setFont(new Font("Calibri", Font.BOLD, 50));
            int x = (this.getWidth() - g2.getFontMetrics().stringWidth("WELCOME TO MEMORY")) / 2;
            int y = (this.getHeight() - g2.getFontMetrics().getHeight()) / 2;
            g2.drawString("WELCOME TO MEMORY", x, y);
            g2.setFont(new Font("Calibri", Font.BOLD, 35));
            g2.setColor(textColor2);
            g2.drawString("MOUSECLICK TO START PLAYING", x, y + 50); 
        } else if (view.getGameState() == GameState.ACTIVE_GAME){
            double margin = 2;
            double width = this.getWidth() - 2 * margin;
            double height = this.getHeight() - 2 * margin;
            Rectangle2D box = new Rectangle2D.Double(margin, margin, width, height);
            GridDimension gd = view.getDimension();
            CellPositionToPixelConverter converter = new CellPositionToPixelConverter(box, gd, margin);
            g2.setColor(colorTheme.getFrameColor());
            g2.fill(box);
            drawCells(g2, view.getPictures(), converter, colorTheme);
        } else if (view.getGameState() == GameState.GAME_OVER){
            g2.setColor(pageColor);
            g2.fillRect(0, 0, this.getWidth(), this.getHeight());
            g2.setColor(textColor);
            g2.setFont(new Font("Calibri", Font.BOLD, 50));
            int x = (this.getWidth() - g2.getFontMetrics().stringWidth("YOU WON")) / 2;
            int y = (this.getHeight() - g2.getFontMetrics().getHeight()) / 2;
            g2.drawString("YOU WON", x, y);
            g2.setFont(new Font("Calibri", Font.BOLD, 35));
            g2.setColor(textColor2);
            g2.drawString("Time: " + view.getTimeUsed() + " seconds", x, y + 50); 
        }
    }

     /**
     * Draws the cells of the board onto the Graphics2D object. 
     * @param g2 the Graphics2D object to draw the cells on
     * @param picture the cells to be drawn
     * @param converter fits the cells to g2
     * @param colorTheme gives the color theme of the board
     */
    private void drawCells(Graphics2D g2, Iterable<GridCell<Character>> cells, CellPositionToPixelConverter converter, ColorTheme colorTheme){
   
        for (GridCell<Character> cell : cells) {
            Rectangle2D rectangle = converter.getBoundsForCell(cell.pos());
            switch(cell.value()){
                case '1':
                    double scale1 = (rectangle.getHeight() - 1) / pictureList.get(0).getHeight(); // å ha scalen inni if-setningen gjør at kjøretiden blir bedre.
                    Inf101Graphics.drawImage(g2, pictureList.get(0), rectangle.getX() + 1, rectangle.getY() + 1, scale1);
                    break;
                case '2':
                    double scale2 = (rectangle.getHeight() - 1) / pictureList.get(1).getHeight();
                    Inf101Graphics.drawImage(g2, pictureList.get(1), rectangle.getX() + 1, rectangle.getY() + 1, scale2);
                    break;
                case '3':
                    double scale3 = (rectangle.getHeight() - 1) / pictureList.get(2).getHeight();
                    Inf101Graphics.drawImage(g2, pictureList.get(2), rectangle.getX() + 1, rectangle.getY() + 1, scale3);
                    break;
                case '4':
                    double scale4 = (rectangle.getHeight() - 1) / pictureList.get(3).getHeight();
                    Inf101Graphics.drawImage(g2, pictureList.get(3), rectangle.getX() + 1, rectangle.getY() + 1, scale4);
                    break;
                case '5':
                    double scale5 = (rectangle.getHeight() - 1) / pictureList.get(4).getHeight();
                    Inf101Graphics.drawImage(g2, pictureList.get(4), rectangle.getX() + 1, rectangle.getY() + 1, scale5);
                    break;
                case '6':
                    double scale6 = (rectangle.getHeight() - 1) / pictureList.get(5).getHeight();
                    Inf101Graphics.drawImage(g2, pictureList.get(5), rectangle.getX() + 1, rectangle.getY() + 1, scale6);
                    break;
                case '7':
                    double scale7 = (rectangle.getHeight() - 1) / pictureList.get(6).getHeight();
                    Inf101Graphics.drawImage(g2, pictureList.get(6), rectangle.getX() + 1, rectangle.getY() + 1, scale7);
                    break;
                case '8':
                    double scale8 = (rectangle.getHeight() - 1) / pictureList.get(7).getHeight();
                    Inf101Graphics.drawImage(g2, pictureList.get(7), rectangle.getX() + 1, rectangle.getY() + 1, scale8);
                    break;
                case'0':
                    Color color = colorTheme.getCellColor();
                    g2.setColor(color);
                    g2.fill(rectangle);
                    break;
            }
        }
    }   
}
    