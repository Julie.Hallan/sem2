package no.uib.inf101.sem2.Model;

import no.uib.inf101.sem2.grid.Grid;

public class MemoryBoard extends Grid<Character> {

    public MemoryBoard(int rows, int cols) {
        super(rows, cols, '0'); //initiates the board with start values.
    }

}