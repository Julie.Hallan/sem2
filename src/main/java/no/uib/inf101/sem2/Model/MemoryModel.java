package no.uib.inf101.sem2.Model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

import no.uib.inf101.sem2.Controller.ControllableMemoryModel;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;
import no.uib.inf101.sem2.view.ViewableMemoryView;


public class MemoryModel implements ViewableMemoryView, ControllableMemoryModel {
    private MemoryBoard board;
    private GameState gameState;
    private final ArrayList<Character> pictureOrder;
    private int score;
    private ArrayList<Character> comparingList;
    private int timeUsed;

    public MemoryModel (MemoryBoard board){
        this.board = board;
        this.gameState = GameState.GAME_START;
        this.pictureOrder = new ArrayList<>(Arrays.asList('1','1','2','2','3','3','4','4','5','5','6','6','7','7','8','8')); // husk å skru på randomisering igjen!!!
        Collections.shuffle(this.pictureOrder);
        this.score = 0;
        this.comparingList = new ArrayList<>();
        this.timeUsed = 0;
    }

    @Override
    public GridDimension getDimension() {
        return board;
    }

    @Override
    public Iterable<GridCell<Character>> getPictures() {
        return board; 
    }

    @Override
    public GameState getGameState() {
        return gameState;
    }

    @Override
    public void openCard(CellPosition position) {
        board.set(position, pictureOrder.get(locationToIndex(position)));
    }

    @Override
    public void closeCard(CellPosition position) {
        board.set(position, '0');
    }
    
    private int locationToIndex(CellPosition position) {
        return position.row() + position.col() * board.rows();
    }

    @Override
    public int addScore() {
        score += 1;
        return score;
    }

    @Override
    public Character cardValue(CellPosition position) {
        int index = locationToIndex(position);
        Character value = pictureOrder.get(index);
        return value;
    }

    private void compareValues(CellPosition position, CellPosition previousPosition) {
        if (comparingList.get(0) == comparingList.get(1)) {
            score += 1;
        } else {
            closeCardDelayed(position, previousPosition);
        }
        comparingList.clear();
    }

    //skrevet med hjelp av ChatGPT
    private void closeCardDelayed(CellPosition position, CellPosition previousPosition) {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                closeCard(position);
                closeCard(previousPosition);
            }
        }, 1000);
    }

    @Override
    public void compareAndAddValues(CellPosition position, CellPosition previousPosition){
        if (comparingList.size() == 1){
            comparingList.add(cardValue(position));
            compareValues(position, previousPosition);
        } else {
            comparingList.add(cardValue(position));
        } 
        gameStateChecker();
    }

    @Override
    public int millisecsInClock() {
        int millisecs = 1000;
        return millisecs;
    }

    @Override
    public void gameStateChecker(){
        if (score == 8){
            gameState = GameState.GAME_OVER;
    }}

    @Override
    public void increaseTimeUsed() {
        timeUsed += 1;
    }

    @Override
    public int getTimeUsed() {
        return timeUsed;
    }

    @Override
    public void startGame() {
        gameState = GameState.ACTIVE_GAME;
    }
    
}
