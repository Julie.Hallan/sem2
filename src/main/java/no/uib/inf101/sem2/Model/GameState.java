package no.uib.inf101.sem2.Model;

public enum GameState {
    ACTIVE_GAME, GAME_OVER, GAME_START;
}
