package no.uib.inf101.sem2.grid;

import java.util.ArrayList;
import java.util.Iterator;

public class Grid<E> implements IGrid<E> {

    private int cols;
    private int rows;
    private ArrayList<ArrayList<E>> grid;

    public Grid(int rows, int cols, E initElement){
        this.rows = rows;
        this.cols = cols;

        this.grid = new ArrayList<>();
        for (int i = 0; i < rows; i++) {
            ArrayList<E> row = new ArrayList<>();
            for (int j = 0; j < cols; j++) {
                row.add(initElement);
            }
        this.grid.add(row);
        }
    }
    /**
     * Creates a new grid object with a given number of rows and columns. 
     * 
     * @param rows number of rows in grid
     * @param cols number of columns in grid
     */
    public Grid(int rows, int cols){
        this(rows, cols, null);
        
    }

    @Override
    public int rows() {
        return rows;
    }

    @Override
    public int cols() {
        return cols;
    }

    @Override
    public Iterator<GridCell<E>> iterator() {
        ArrayList<GridCell<E>> iteratorItems = new ArrayList<>();   
        for (int i = 0; i < rows(); i++){
            for (int j = 0; j < cols(); j++) {
                iteratorItems.add(new GridCell<E>(new CellPosition(i,j), grid.get(i).get(j)));
            }
        }
        return iteratorItems.iterator();
    }

    @Override
    public void set(CellPosition pos, E value) {
        int row = pos.row();
        int col = pos.col();
        ArrayList<E> thisRow = grid.get(row);
        thisRow.set(col, value);

    }

    @Override
    public E get(CellPosition pos) {
        int x = pos.col();
        int y = pos.row();
        return grid.get(y).get(x);

    }

    @Override
    public boolean positionIsOnGrid(CellPosition pos) {
        int x = pos.col(); 
        int y = pos.row(); 
        if (x >= 0 && x < this.cols() && y >= 0 && y < this.rows()){
            return true;
        }
        
        return false;

    }
    @Override
    public int getCols() {
        return cols;
    }


    
}
