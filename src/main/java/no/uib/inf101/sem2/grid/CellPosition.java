package no.uib.inf101.sem2.grid;

/**
 * A CellPosition consists of a row and a column
 * 
 * @param row
 * @param col
 */

public record CellPosition(int row, int col) {

    /**
     * Returns a new CellPosition object shifted by the given row and column values. 
     * 
     * @param deltaRow the number of rows to shift the CellPosition by
     * @param deltaCol the number of columns to shift the CellPosition by
     * @return the new CellPosition object
     */
    //Brukes ikke, kan fjernes
    public CellPosition shiftedBy(int deltaRow, int deltaCol) {
        return new CellPosition(row + deltaRow, col + deltaCol);
    }
}
