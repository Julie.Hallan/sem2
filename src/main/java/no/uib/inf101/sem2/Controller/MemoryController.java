package no.uib.inf101.sem2.Controller;

import java.awt.event.MouseEvent;
import javax.swing.Timer;
import java.awt.event.ActionEvent;

import no.uib.inf101.sem2.Model.GameState;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.view.MemoryView;

public class MemoryController implements java.awt.event.MouseListener{
    private final ControllableMemoryModel model;
    private final MemoryView view;
    private CellPosition previousPosition;
    Timer timer;
    private int timeUsed;

    public MemoryController(ControllableMemoryModel model, MemoryView view){
        this.model = model;
        this.view = view;
        view.addMouseListener(this);
        this.timer = new Timer(model.millisecsInClock(), this::clockTick);
        timer.start();
        this.timeUsed = 0;
    }
    
    @Override
    public void mouseClicked(MouseEvent e) { 
        // TODO Auto-generated method stub 
    }

    private CellPosition getCoordinate(int x, int y){
        int row = y * model.getDimension().rows()/view.getHeight();
        int col = x * model.getDimension().cols()/view.getWidth();
        return (new CellPosition(row, col));
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (model.getGameState() == GameState.GAME_START){
            model.startGame();
        }
        CellPosition position = getCoordinate(e.getX(), e.getY());
        if (model.getGameState() == GameState.ACTIVE_GAME){
            view.repaint();
            model.openCard(position); 
            model.compareAndAddValues(position, previousPosition);
            previousPosition = position;
        }
        view.repaint(); 
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub
    }
 
    public void clockTick(ActionEvent aE){
        if (model.getGameState() == GameState.ACTIVE_GAME){
            model.increaseTimeUsed();
            view.repaint();
        }
    }

    public int getTime() {
        return timeUsed;
    }
}