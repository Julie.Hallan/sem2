package no.uib.inf101.sem2.Controller;

import no.uib.inf101.sem2.Model.GameState;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridDimension;

public interface ControllableMemoryModel {

    /**
     * Returns the current GameState.
     * @return object of the type GameState
     */
    GameState getGameState();

    /**
     * Returns the dimensions of the game board as a GridDimension object.
     * @return the dimensions of the game board
     */
    GridDimension getDimension();

    /**
     * Sets the value of a given position on the board to the value of a given picture
     * from a set list, in order to reveal the picture on the card.
     * 
     * @param position the position of the card to be revealed
     */
    void openCard(CellPosition cellPosition);

    /**
     * Increases the players score every time it is called.
     * @return object with score.
     */
    int addScore();

    /**
     * Gets the index of the position of the board and locates the value for the position.
     * @param position position of the card to find the value of.
     * @return the value of the card.
     */
    Character cardValue(CellPosition position);

    /**
     * Resets the card to the original value, to hide the picture. 
     * @param position
     */
    void closeCard(CellPosition position);

    /**
     * Compares and adds the values of two cell positions to a comparing list, and checks the game state.
     * @param position the first cell position to compare and add
     * @param previousPosition the second cell position to compare and add
     */
    void compareAndAddValues(CellPosition position, CellPosition previousPosition);

    /**
     * Returns the number of milliseconds in the clock.
     *
     * @return the number of milliseconds in the clock
     */
    int millisecsInClock();

    /**
     * Increases a variable by the amount of time used by the player every second.
     */
    void increaseTimeUsed();

    /**
     * When called swished the GameState to ACTIVE_GAME in order to start the game. 
     */
    void startGame();

}
